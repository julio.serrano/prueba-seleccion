var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var characters = require('../models/characters')

/*
router.get('/', function (req, res) {
    characters.find({}, function(err, characters){
      if(err){
        console.log(err);
      } else{
        res.send(characters)
      }
  })
});
*/

router.get('/',(req,res) => {
  var page = parseInt(req.query.page)
  var size = parseInt(req.query.size)
  var query = {}
  
  if(page <= 0) {
    return res.json({"error" : true, "message" : "invalid page number"})
  }
  
  query.skip = size * (page - 1)
  query.limit = size
  
  // Find some documents
  characters.count({},function(err,totalCount) {

      characters.find({},{},query,function(err,data) {

      if(err) {
        response = {"error" : true,"message" : "Error fetching data"};
      }
      else {
        var totalPages = Math.ceil(totalCount / size)
        response = {data,totalPages};
      }
      res.json(response);
    });
  })
})

router.get('/:id', (req,res)=>{
  const id = req.params
  characters.find(id, (err, character)=>{
    if(err){
      res.send(err)
    }
    else{
      res.send(character)
    }
  })
})

module.exports = router
