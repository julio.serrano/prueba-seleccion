


var express = require('express')
var router = express.Router()
var request = require('request');

var characters = require('../models/characters')

router.get('/', function(req, res){

  var numCharacters

  characters.count({}, function( err, count){
    numCharacters = count
  })

  console.log(numCharacters)

  if(numCharacters === 0 || numCharacters=== undefined){
    request('https://api.got.show/api/show/characters', function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var info = JSON.parse(body)
        characters.collection.insert(info, function (err, docs) {
          if (err){ 
            return console.error(err);
          } else {
            console.log("Multiple documents inserted to Collection");
          }
        });
        res.send(info)
      }
    })
  }
  else{
    res.send('characters already extracted!')
  }
})

module.exports = router;