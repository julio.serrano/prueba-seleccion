var express = require('express');
var extractCharacters = require('./routes/extractCharacters')
var characters = require('./routes/characters')
var db = require('./config/db')

const app = express();

app.listen(3001, () =>
  console.log('Example app listening on port 3001!'),
);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//extract characters from api
app.use('/extractCharacters', extractCharacters)
app.use('/characters',characters)

db.on('error', console.error.bind(console, 'connection error:')); 
db.once('open', function() {
  console.log("Connection Successful!"); 
})

