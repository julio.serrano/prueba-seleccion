import React, {Component} from 'react';
import './styles.css'

import {connect} from 'react-redux'
import {getOneItem} from '../../redux/actions/getCharacters'

class DetailsCharacter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: '',
        };

    }
    
    componentDidMount(){
        this.props.getOneItem(this.props.actualCharacterId)
    }



    render() {
        const {
            item
        } = this.props;
        var character = item[0]
        return (

            <div>
                {character !== undefined ?
                
                <div className="data-list">
                    <img style={{margin:"auto",display:"block",marginBlockEnd:20}} src={character.image}/>
                    <p className="data"><b>Nombre:</b> {character.name}</p>
                    <p className="data"><b>Casa:</b> {character.house}</p>
                    <p className="data"><b>Género:</b> {character.gender}</p>
                    <p className="data"><b>Títulos: </b>
                    {character.titles.map((item,i)=>{
                        return(
                        <li kei={i}>{item}</li>
                        )
                    })}</p>
                    <p className="data"><b>Ranking:</b> {character.pagerank.rank}</p>
                </div>
                :
                null
                }

            </div>
           
        )
    }
}

const mapStateToProps = (state) => {
    return {
        item: state.oneCharacter,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getOneItem: (id) => dispatch(getOneItem(id))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(DetailsCharacter);