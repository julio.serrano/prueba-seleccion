import React, {Component} from 'react';
import Page from './page';

import {connect} from 'react-redux'
import {itemsFetchData} from '../../redux/actions/getCharacters'
import {getAllItems} from '../../redux/actions/getCharacters'
import findSuggestions from '../../redux/actions/findSuggestions'



class Characters extends Component {

    constructor(props) {
        super(props);

        this.state = {
            currentPage: 0,
            text: '',
            suggestionsCharacters:[]
        }
        this.handleClick = this.handleClick.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
    }

    componentDidMount(){
        this.props.fetchData(1);
        this.props.fetchAll()
    }

    
    handleClick(e, index) {
        
        e.preventDefault();
        this.setState({
          currentPage: index
        });

        if(this.state.text === ''){
            this.props.fetchData(index+1)
        }
        else {
            this.setState({suggestionsCharacters:this.pagination(this.props.suggestions,index)})
        }
    }


    pagination(array,index){
        return array.slice(index*10,(index+1)*10)
    }

    async onChangeText(text) {
        this.setState({ text, currentPage:0});
        let object = {text,characters:this.props.allItems}
        await this.props.findSuggestions(object)
        this.setState({suggestionsCharacters: this.pagination(this.props.suggestions, 0)})
        
    }

    render(){
        const { currentPage, text } = this.state;
        const {items, suggestions} = this.props;
        const data = items.data
        const pagesCount = items.totalPages;


        return(
            <div>
            {text === '' ?
                <Page  
                    data = {data}
                    text = {text}
                    pagesCount = {pagesCount}
                    currentPage = {currentPage}
                    onChangeText = {this.onChangeText}
                    handleClick = {this.handleClick}
                />
                :
                <Page
                    data = {this.state.suggestionsCharacters}
                    text = {text}
                    pagesCount = {Math.ceil(suggestions.length/10)}
                    currentPage = {currentPage}
                    onChangeText = {this.onChangeText}
                    handleClick = {this.handleClick}
                />
            }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.characters,
        suggestions: state.suggestions,
        allItems: state.allCharacters
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (page) => dispatch(itemsFetchData(page)),
        fetchAll: () => dispatch(getAllItems()),
        findSuggestions: (obj) => dispatch(findSuggestions(obj))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Characters);
   