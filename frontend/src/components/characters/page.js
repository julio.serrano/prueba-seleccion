import React from 'react';
import './styles.css'
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import CharactersList from '../charactersList';

function Page(props) {

    const {
        currentPage,
        data,
        pagesCount,
        handleClick,
        onChangeText,
        text,

    } = props;

    return(
        <React.Fragment>
            {data !== undefined ?
                <CharactersList
                    characters = {data}
                    onChangeText = {onChangeText}
                    text = {text}
                />
                : null
            }
    
            <div className="pagination-wrapper">
                <Pagination className="pagination">
                    <PaginationItem disabled={currentPage <= 0}>
                        <PaginationLink
                            onClick={e => handleClick(e, currentPage - 1)}
                            previous
                            href="#"
                        />
                    </PaginationItem>

                    {[...Array(pagesCount)].map((page, i) => 
                        <PaginationItem active={i === currentPage} key={i}>
                            <PaginationLink onClick={e => handleClick(e, i)} href="#">
                            {i + 1}
                            </PaginationLink>
                        </PaginationItem>
                    )}

                    <PaginationItem disabled={currentPage >= pagesCount -1}>
                        <PaginationLink
                            onClick={e => handleClick(e, currentPage + 1)}
                            next
                            href="#"
                        />
                    </PaginationItem>
                </Pagination>
            </div>
        </React.Fragment>
    )
}

export default Page;