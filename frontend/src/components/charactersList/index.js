import React, {Component} from 'react';

import {
	Col,
	Table,
	Form,
    FormGroup,
    Input,
    Button,
    Modal,
    ModalHeader,
    ModalBody
} from "reactstrap"

import './styles.css'
import DetailsCharacter from '../detailsCharacter';


class CharactersList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: '',
            open: false,
            actualCharacterId:''
        }
    }
    
    componentDidMount(){
        this.setState({text:this.props.text})
    }

    openModal(id){
        this.setState({
            open: true,
            actualCharacterId: id
        })
    }

    closeModal(){
        this.setState({
            open:false
        })
    }

    render() {
        const {
            onChangeText,
            characters,
            text
        } = this.props;

        return (
            <div>
                <Modal isOpen={this.state.open} onClick={()=> this.closeModal()} >
                    <ModalHeader toggle={()=>this.closeModal()}>
                        Detalles personaje
                    </ModalHeader>
                    <ModalBody>

                        <DetailsCharacter
                            actualCharacterId={this.state.actualCharacterId}
                        />

                    </ModalBody>
                </Modal>
                

                <Col lg="4" style={{paddingTop:20}}>
                    <Form>
                        <FormGroup>
                            <Input type="text" className="form-control form-control-lg" placeholder="Buscar" value={text} onChange = {(event) => {onChangeText(event.target.value)}}/>
                        </FormGroup>
                    </Form>  
                </Col>

                <div className="table">
                    <Table striped>
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Casa</th>
                                <th>Acción</th> 
                            </tr>
                        </thead>
                        <tbody>
                            {characters.map((item,index)=>{
                                return(
                                    <tr key={index}>
                                        <td>{item.name}</td>
                                        <td>{item.house}</td>
                                        <td>                        
                                            <Button onClick={()=> this.openModal(item.id)}>
                                                Ver detalles
                                            </Button>                                               
                                        </td> 
                                    </tr>
                                )
                            })}
                        </tbody>
                    </Table>
                </div>
            </div>
        )
    }
}




export default CharactersList;