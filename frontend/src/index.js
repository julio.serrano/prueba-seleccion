
import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import { BrowserRouter, Route, Redirect, Switch} from 'react-router-dom'
import Characters from './components/characters'
import store from './redux/store'

import 'bootstrap/dist/css/bootstrap.min.css';


const Root = (
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route path="/characters" component={Characters} />
                <Redirect from="/" to="/characters" />
            </Switch>
        </BrowserRouter>
    </Provider>
)



ReactDOM.render(Root, document.getElementById('root'));