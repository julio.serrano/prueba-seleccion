
const defaultState = null;

function reducer(state = defaultState, action) {

    switch(action.type){
        case "findSuggestions":{
            if (!action.payload) {
                return [];
            }            

            var nameList = action.payload.characters.filter(function(item){
                return item.name.toLowerCase().search(
                    action.payload.text.toLowerCase()) !== -1;
            });
            var houseList = action.payload.characters.filter(function(item){
                if(item.house !== undefined){
                    return item.house.toLowerCase().search(
                        action.payload.text.toLowerCase()) !== -1;
                    }
                }
            );
            return houseList.concat(nameList)
        }
        default:
            return state;
    }
}

export default reducer;