
function reducer(state = [], action) {
    switch (action.type) {
        case 'ITEMS_FETCH_ONE_SUCCESS':
            return action.items;

        default:
            return state;
    }
}

export default reducer;