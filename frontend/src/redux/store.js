import {createStore, combineReducers, applyMiddleware} from 'redux'
import thunk from 'redux-thunk';
import characters from './reducers/characters'
import suggestions from './reducers/suggestions'
import allCharacters from './reducers/allCharacters'
import oneCharacter from './reducers/oneCharacter'

const reducers = combineReducers({
    allCharacters,
    characters,
    suggestions,
    oneCharacter
})
const store = createStore(reducers, applyMiddleware(thunk));

export default store;