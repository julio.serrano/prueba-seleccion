
export function itemsFetchDataSuccess(items) {
    return {
        type: 'ITEMS_FETCH_DATA_SUCCESS',
        items
    };
}

export function itemsFetchAllSuccess(items) {
    return {
        type: 'ITEMS_FETCH_ALL_SUCCESS',
        items
    };
}

export function itemFetchOneSuccess(items) {
    return {
        type: 'ITEMS_FETCH_ONE_SUCCESS',
        items
    };
}

export function itemsFetchData(page) {
    return (dispatch) => {
        fetch("http://localhost:3001/characters?page="+ page +"&size=10")
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((items) => dispatch(itemsFetchDataSuccess(items)))
    };
}

export function getAllItems(){
    return (dispatch) => {
        fetch("http://localhost:3001/characters")
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((items) => dispatch(itemsFetchAllSuccess(items.data)))
    };
}

export function getOneItem(id){
    return (dispatch) => {
        fetch("http://localhost:3001/characters/"+ id)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((item) => dispatch(itemFetchOneSuccess(item)))
    };
}