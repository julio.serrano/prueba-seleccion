export const type = 'findSuggestions'

const findSuggestions = array => {
    return {
        type,
        payload: array,
    }
}

export default findSuggestions;